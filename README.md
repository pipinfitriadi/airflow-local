<!-- Copyright 2022 Pipin Fitriadi <pipinfitriadi@gmail.com>

Licensed under the Microsoft Reference Source License (MS-RSL)

This license governs use of the accompanying software. If you use the
software, you accept this license. If you do not accept the license, do not
use the software.

1. Definitions

The terms "reproduce," "reproduction" and "distribution" have the same
meaning here as under U.S. copyright law.

"You" means the licensee of the software.

"Your company" means the company you worked for when you downloaded the
software.

"Reference use" means use of the software within your company as a reference,
in read only form, for the sole purposes of debugging your products,
maintaining your products, or enhancing the interoperability of your
products with the software, and specifically excludes the right to
distribute the software outside of your company.

"Licensed patents" means any Licensor patent claims which read directly on
the software as distributed by the Licensor under this license.

2. Grant of Rights

(A) Copyright Grant- Subject to the terms of this license, the Licensor
grants you a non-transferable, non-exclusive, worldwide, royalty-free
copyright license to reproduce the software for reference use.

(B) Patent Grant- Subject to the terms of this license, the Licensor grants
you a non-transferable, non-exclusive, worldwide, royalty-free patent
license under licensed patents for reference use.

3. Limitations

(A) No Trademark License- This license does not grant you any rights to use
the Licensor's name, logo, or trademarks.

(B) If you begin patent litigation against the Licensor over patents that
you think may apply to the software (including a cross-claim or counterclaim
in a lawsuit), your license to the software ends automatically.

(C) The software is licensed "as-is." You bear the risk of using it. The
Licensor gives no express warranties, guarantees or conditions. You may have
additional consumer rights under your local laws which this license cannot
change. To the extent permitted under your local laws, the Licensor excludes
the implied warranties of merchantability, fitness for a particular purpose
and non-infringement. -->

# Airflow di Komputer Lokal

## Cara menambahkan _repository_ ini menjadi Git Submodule ke _repository project_ kita

```sh
git submodule add https://gitlab.com/pipinfitriadi/airflow-local.git airflow/local
```

## Cara Menggunakan Airflow

1. Pastikan [Docker Desktop](https://www.docker.com/products/docker-desktop/) sudah berjalan pada komputer.
2. Pastikan lokasi _folder_ pada _terminal_ sudah ada di `{root_repo}/airflow/local/` dan sudah ada _file_ `{root_repo}/airflow/dags/{dag_nama_file}.py`.
3. Jalankan perintah Docker Compose berikut ini di _terminal_ komputer:

    ```shell
    docker compose build  # Only at first time
    docker compose up airflow-init  # Only at first time
    docker compose up -d
    ```

    > Referensi dari [sini](https://airflow.apache.org/docs/apache-airflow/stable/howto/docker-compose/index.html).

4. Isi _username_ dan _password_ dengan `airflow` untuk _login_ lalu akses Airflow di [http://localhost:8080/](http://localhost:8080/).

    > _File_ `{root_repo}/airflow/dags/{dag_nama_file}.py` dapat diubah saat _service_ Airflow masih berjalan.

5. Buat _file_ `{root_repo}/airflow/local/.env` berdasarkan [`template.env`](template.env).

6. Apabila Airflow sudah tidak digunakan, jalankan perintah Docker Compose berikut ini di _terminal_ komputer untuk menghentikannya:

    ```shell
    docker compose down -v --remove-orphans
    ```

## Lisensi

Lisensi yang dipergunakan adalah [MS-RSL](LICENSE) (Microsoft Reference Source License).
